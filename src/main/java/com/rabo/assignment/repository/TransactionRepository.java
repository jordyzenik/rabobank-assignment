package com.rabo.assignment.repository;

import com.rabo.assignment.model.Transaction;
import com.rabo.assignment.model.enums.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findByTypeEquals(TransactionType type);

}
