package com.rabo.assignment.repository;

import com.rabo.assignment.model.AccountHolder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountHolderRepository extends JpaRepository<AccountHolder, Long> {

    Optional<AccountHolder> findByAccounts_Id(long id);


}
