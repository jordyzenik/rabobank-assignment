package com.rabo.assignment.repository;

import com.rabo.assignment.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRepository extends JpaRepository<Card, Long> {
}
