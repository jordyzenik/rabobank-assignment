package com.rabo.assignment.controller;

import com.rabo.assignment.model.Account;
import com.rabo.assignment.model.AccountHolder;
import com.rabo.assignment.model.dto.BalanceDto;
import com.rabo.assignment.repository.AccountHolderRepository;
import com.rabo.assignment.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountHolderRepository accountHolderRepository;


    @GetMapping
    public ResponseEntity<List<Account>> getAllAccounts() {
        try {
            List<Account> accounts = accountRepository.findAll();

            return new ResponseEntity<>(accounts, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/balances")
    public ResponseEntity<List<BalanceDto>> getAllAccountBalances() {
        try {
            List<BalanceDto> balances = new ArrayList<>();

            List<Account> accounts = accountRepository.findAll();

            accounts.forEach((account) -> {
                Optional<AccountHolder> accountHolder = accountHolderRepository.findByAccounts_Id(account.getId());

                BalanceDto balance = new BalanceDto(account.getId(), accountHolder.get().getFirstNames(), accountHolder.get().getLastName(), account.getIban(), account.getBalance());
                balances.add(balance);
            });


            return new ResponseEntity<>(balances, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
