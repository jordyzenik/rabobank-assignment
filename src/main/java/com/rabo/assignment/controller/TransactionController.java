package com.rabo.assignment.controller;

import com.rabo.assignment.model.Account;
import com.rabo.assignment.model.Exceptions.EntityNotPresentException;
import com.rabo.assignment.model.Exceptions.InsufficientFundsException;
import com.rabo.assignment.model.Transaction;
import com.rabo.assignment.model.enums.TransactionType;
import com.rabo.assignment.repository.AccountRepository;
import com.rabo.assignment.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping
    public ResponseEntity<List<Transaction>> getAllTransactions() {
        try {
            List<Transaction> transactions = transactionRepository.findAll();

            return new ResponseEntity<>(transactions, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/withdrawals")
    public ResponseEntity<List<Transaction>> getAllWithdrawals() {
        try {
            List<Transaction> withdrawals = transactionRepository.findByTypeEquals(TransactionType.WITHDRAWAL);

            return new ResponseEntity<>(withdrawals, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/transfers")
    public ResponseEntity<List<Transaction>> getAllTransfers() {
        try {
            List<Transaction> transfers = transactionRepository.findByTypeEquals(TransactionType.TRANSFER);

            return new ResponseEntity<>(transfers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/withdraw")
    public ResponseEntity<String> withdrawMoney(@RequestParam long accountId, @RequestParam BigDecimal amount) {
        try {
            Optional<Account> account = accountRepository.findById(accountId);

            validateAccountExists(account);
            validateSufficientBalanceAvailable(account.get(), amount);

            BigDecimal currentBalance = account.get().getBalance();
            BigDecimal newBalance = currentBalance.subtract(amount);

            Transaction transaction = new Transaction(amount, account.get(), null, TransactionType.WITHDRAWAL, LocalDateTime.now());
            transactionRepository.save(transaction);

            account.get().setBalance(newBalance);
            accountRepository.save(account.get());

            return new ResponseEntity<>("Geld opgenomen. \nHuidige balans: " + currentBalance + " Nieuwe balans: " + newBalance, HttpStatus.OK);

        } catch (EntityNotPresentException e) {
            return new ResponseEntity<>("Account bestaat niet.", HttpStatus.OK);
        } catch (InsufficientFundsException e) {
            return new ResponseEntity<>("Saldo ontoereikend.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/transfer")
    public ResponseEntity<String> transferMoney(long accountIdFrom, long accountIdTo, BigDecimal amount) {
        if (accountIdFrom == accountIdTo) {
            return new ResponseEntity<>("Je mag geen geld naar jezelf overmaken.", HttpStatus.BAD_REQUEST);
        }

        try {
            Optional<Account> accountFrom = accountRepository.findById(accountIdFrom);
            Optional<Account> accountTo = accountRepository.findById(accountIdTo);

            validateAccountExists(accountFrom);
            validateAccountExists(accountTo);
            validateSufficientBalanceAvailable(accountFrom.get(), amount);

            BigDecimal currentBalanceFrom = accountFrom.get().getBalance();
            BigDecimal newBalanceFrom = currentBalanceFrom.subtract(amount);

            BigDecimal currentBalanceTo = accountTo.get().getBalance();
            BigDecimal newBalanceTo = currentBalanceTo.add(amount);

            Transaction transaction = new Transaction(amount, accountFrom.get(), accountTo.get(), TransactionType.TRANSFER, LocalDateTime.now());
            transactionRepository.save(transaction);

            accountFrom.get().setBalance(newBalanceFrom);
            accountRepository.save(accountFrom.get());

            accountTo.get().setBalance(newBalanceTo);
            accountRepository.save(accountTo.get());

            return new ResponseEntity<>("Geld verstuurd. " +
                    "\nHuidige balans accountFrom: " + currentBalanceFrom + " Nieuwe balans accountFrom: " + newBalanceFrom +
                    "\nHuidige balans accountTo: " + currentBalanceTo + " Nieuwe balans accountTo: " + newBalanceTo, HttpStatus.OK);

        } catch (EntityNotPresentException e) {
            return new ResponseEntity<>("Account bestaat niet.", HttpStatus.OK);
        } catch (InsufficientFundsException e) {
            return new ResponseEntity<>("Saldo ontoereikend.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private void validateSufficientBalanceAvailable(Account account, BigDecimal amount) throws InsufficientFundsException {
        if (account.getBalance().compareTo(amount) < 0) {
            throw new InsufficientFundsException();
        }
    }

    private void validateAccountExists(Optional<Account> account) throws EntityNotPresentException {
        if (account.isEmpty()) {
            throw new EntityNotPresentException();
        }
    }
}
