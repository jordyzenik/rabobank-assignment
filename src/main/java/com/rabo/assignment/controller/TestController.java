package com.rabo.assignment.controller;

import com.rabo.assignment.model.Account;
import com.rabo.assignment.model.AccountHolder;
import com.rabo.assignment.model.Card;
import com.rabo.assignment.model.Transaction;
import com.rabo.assignment.model.enums.CardStatus;
import com.rabo.assignment.model.enums.Gender;
import com.rabo.assignment.model.enums.TransactionType;
import com.rabo.assignment.repository.AccountHolderRepository;
import com.rabo.assignment.repository.AccountRepository;
import com.rabo.assignment.repository.CardRepository;
import com.rabo.assignment.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/")
public class TestController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private AccountHolderRepository accountHolderRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @PostMapping("/addTestData")
    public ResponseEntity<List<AccountHolder>> addTestData() {
        try {
            Card card1 = new Card(3213, LocalDate.of(2024, 1, 1), CardStatus.OPEN);
            Account account1 = new Account("NLRABO03489789512", new BigDecimal("12023.95"), card1);
            AccountHolder accountHolder1 = new AccountHolder("Jordy", "Zenik", "222222222", "jordy.zenik@blue4it.nl", Gender.MALE, LocalDate.of(1995, 9, 26), "Nederlandse", List.of(account1));

            Card card2 = new Card(6524, LocalDate.of(2025, 9, 26), CardStatus.OPEN);
            Account account2 = new Account("NLRABO03481473512", new BigDecimal("1563.99"), card2);
            AccountHolder accountHolder2 = new AccountHolder("Anita", "Jansen", "333333333", "anita.jansen@gmail.com", Gender.FEMALE, LocalDate.of(1985, 3, 12), "Nederlandse", List.of(account2));

            cardRepository.save(card1);
            account1 = accountRepository.save(account1);
            accountHolderRepository.save(accountHolder1);

            cardRepository.save(card2);
            account2 = accountRepository.save(account2);
            accountHolderRepository.save(accountHolder2);

            Transaction transaction1 = new Transaction(new BigDecimal("45.55"), account1, account2, TransactionType.TRANSFER, LocalDateTime.now());
            Transaction transaction2 = new Transaction(new BigDecimal("150"), account1, null, TransactionType.WITHDRAWAL, LocalDateTime.now().minusDays(1));

            transactionRepository.save(transaction1);
            transactionRepository.save(transaction2);

            return new ResponseEntity<>(accountHolderRepository.findAll(), HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
