package com.rabo.assignment.model.enums;

public enum Gender {
    MALE,
    FEMALE,
    OTHER;
}
