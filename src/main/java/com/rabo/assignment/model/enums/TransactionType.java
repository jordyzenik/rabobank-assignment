package com.rabo.assignment.model.enums;

public enum TransactionType {
    WITHDRAWAL,
    TRANSFER
}
