package com.rabo.assignment.model.enums;

public enum CardStatus {
    OPEN,
    BLOCKED
}
