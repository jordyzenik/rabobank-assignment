package com.rabo.assignment.model.dto;

import java.math.BigDecimal;

public class BalanceDto {
    private long accountId;
    private String firstnames;
    private String lastname;
    private String iban;
    private BigDecimal balance;

    public BalanceDto(long accountId, String firstnames, String lastname, String iban, BigDecimal balance) {
        this.accountId = accountId;
        this.firstnames = firstnames;
        this.lastname = lastname;
        this.iban = iban;
        this.balance = balance;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getFirstnames() {
        return firstnames;
    }

    public void setFirstnames(String firstnames) {
        this.firstnames = firstnames;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BalanceDto that = (BalanceDto) o;
        return accountId == that.accountId && firstnames.equals(that.firstnames) && lastname.equals(that.lastname) && iban.equals(that.iban) && balance.equals(that.balance);
    }
}
