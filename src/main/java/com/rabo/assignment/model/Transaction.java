package com.rabo.assignment.model;

import com.rabo.assignment.model.enums.TransactionType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @NotNull
    private BigDecimal amount;

    @NotNull
    @OneToOne
    private Account from;

    @OneToOne
    private Account to;

    @NotNull
    private TransactionType type;

    @NotNull
    private LocalDateTime date;

    public Transaction(BigDecimal amount, Account from, Account to, TransactionType type, LocalDateTime date) {
        this.amount = amount;
        this.from = from;
        this.to = to;
        this.type = type;
        this.date = date;
    }

    public Transaction() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
