package com.rabo.assignment.controller;

import com.rabo.assignment.model.Account;
import com.rabo.assignment.model.AccountHolder;
import com.rabo.assignment.model.Card;
import com.rabo.assignment.model.dto.BalanceDto;
import com.rabo.assignment.model.enums.CardStatus;
import com.rabo.assignment.model.enums.Gender;
import com.rabo.assignment.repository.AccountHolderRepository;
import com.rabo.assignment.repository.AccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountControllerTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private AccountHolderRepository accountHolderRepository;

    @InjectMocks
    private AccountController controller;

    Account account1;
    Account account2;
    AccountHolder accountHolder1;
    AccountHolder accountHolder2;

    @BeforeEach
    public void setUp() {
        Card card1 = new Card(3213, LocalDate.of(2024, 1, 1), CardStatus.OPEN);
        Card card2 = new Card(6524, LocalDate.of(2025, 9, 26), CardStatus.OPEN);

        account1 = new Account("NLRABO03489789512", new BigDecimal("100"), card1);
        account2 = new Account("NLRABO03481473512", new BigDecimal("100"), card2);

        accountHolder1 = new AccountHolder("Jordy", "Zenik", "222222222", "jordy.zenik@blue4it.nl", Gender.MALE, LocalDate.of(1995, 9, 26), "Nederlandse", List.of(account1));
        accountHolder2 = new AccountHolder("Anita", "Jansen", "333333333", "anita.jansen@gmail.com", Gender.FEMALE, LocalDate.of(1985, 3, 12), "Nederlandse", List.of(account2));

        account1.setId(1);
        account2.setId(2);
    }

    @Test
    public void testWithdrawMoneyEnoughBalance() {
        when(accountRepository.findAll()).thenReturn(List.of(account1, account2));
        when(accountHolderRepository.findByAccounts_Id(account1.getId())).thenReturn(Optional.of(accountHolder1));
        when(accountHolderRepository.findByAccounts_Id(account2.getId())).thenReturn(Optional.of(accountHolder2));

        ResponseEntity<List<BalanceDto>> result = controller.getAllAccountBalances();

        BalanceDto expectedDto1 = new BalanceDto(account1.getId(), accountHolder1.getFirstNames(), accountHolder1.getLastName(), account1.getIban(), account1.getBalance());
        BalanceDto expectedDto2 = new BalanceDto(account2.getId(), accountHolder2.getFirstNames(), accountHolder2.getLastName(), account2.getIban(), account2.getBalance());

        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals(List.of(expectedDto1, expectedDto2), result.getBody());
    }
}
