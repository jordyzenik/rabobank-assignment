package com.rabo.assignment.controller;

import com.rabo.assignment.model.Account;
import com.rabo.assignment.model.Card;
import com.rabo.assignment.model.Transaction;
import com.rabo.assignment.model.enums.CardStatus;
import com.rabo.assignment.model.enums.TransactionType;
import com.rabo.assignment.repository.AccountRepository;
import com.rabo.assignment.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private TransactionController controller;

    Account account1;
    Account account2;

    @BeforeEach
    public void setUp() {
        Card card1 = new Card(3213, LocalDate.of(2024, 1, 1), CardStatus.OPEN);
        Card card2 = new Card(6524, LocalDate.of(2025, 9, 26), CardStatus.OPEN);

        account1 = new Account("NLRABO03489789512", new BigDecimal("100"), card1);
        account2 = new Account("NLRABO03481473512", new BigDecimal("100"), card2);
    }


    @Test
    public void testWithdrawMoneyEnoughBalance() {
        when(accountRepository.findById(any())).thenReturn(Optional.of(account1));
        ResponseEntity<String> result = controller.withdrawMoney(account1.getId(), new BigDecimal("100"));

        // Check that the right response is sent
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Geld opgenomen. \nHuidige balans: 100 Nieuwe balans: 0", result.getBody());

        // Check that the right objects are saved
        account1.setBalance(new BigDecimal("0"));
        verify(transactionRepository, times(1)).save(new Transaction(new BigDecimal("100"), account1, null, TransactionType.WITHDRAWAL, any()));
        verify(accountRepository, times(1)).save(account1);
    }

    @Test
    public void testWithdrawMoneyNotEnoughBalance() {
        when(accountRepository.findById(any())).thenReturn(Optional.of(account1));
        ResponseEntity<String> result = controller.withdrawMoney(account1.getId(), new BigDecimal("100.01"));

        // Check that the right response is sent
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Saldo ontoereikend.", result.getBody());

        // Check that there are no objects saved
        verify(transactionRepository, never()).save(any());
        verify(accountRepository, never()).save(any());
    }

    @Test
    public void testWithdrawMoneyAccountNotPresent() {
        when(accountRepository.findById(any())).thenReturn(Optional.empty());
        ResponseEntity<String> result = controller.withdrawMoney(account1.getId(), new BigDecimal("99.99"));

        // Check that the right response is sent
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Account bestaat niet.", result.getBody());

        // Check that there are no objects saved
        verify(transactionRepository, never()).save(any());
        verify(accountRepository, never()).save(any());
    }

    @Test
    public void testTransferMoneyEnoughBalance() {
        when(accountRepository.findById(1L)).thenReturn(Optional.of(account1));
        when(accountRepository.findById(2L)).thenReturn(Optional.of(account2));
        ResponseEntity<String> result = controller.transferMoney(1, 2, new BigDecimal("100.00"));

        // Check that the right response is sent
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Geld verstuurd. \n" +
                "Huidige balans accountFrom: 100 Nieuwe balans accountFrom: 0.00\n" +
                "Huidige balans accountTo: 100 Nieuwe balans accountTo: 200.00", result.getBody());

        // Check that the right objects are saved
        Account account1WithNewBalance = account1;
        account1WithNewBalance.setBalance(new BigDecimal("0"));

        Account account2WithNewBalance = account2;
        account2WithNewBalance.setBalance(new BigDecimal("200"));

        verify(transactionRepository, times(1)).save(new Transaction(new BigDecimal("100"), account1, account2, TransactionType.WITHDRAWAL, any()));
        verify(accountRepository, times(1)).save(account1WithNewBalance);
        verify(accountRepository, times(1)).save(account2WithNewBalance);
    }

    @Test
    public void testTransferMoneyNotEnoughBalance() {
        when(accountRepository.findById(1L)).thenReturn(Optional.of(account1));
        when(accountRepository.findById(2L)).thenReturn(Optional.of(account2));
        ResponseEntity<String> result = controller.transferMoney(1, 2, new BigDecimal("100.01"));

        // Check that the right response is sent
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Saldo ontoereikend.", result.getBody());

        // Check that there are no objects saved
        verify(transactionRepository, never()).save(any());
        verify(accountRepository, never()).save(any());
    }

    @Test
    public void testTransferMoneyAccountNotPresent() {
        when(accountRepository.findById(1L)).thenReturn(Optional.empty());
        when(accountRepository.findById(2L)).thenReturn(Optional.of(account2));
        ResponseEntity<String> result = controller.transferMoney(1, 2, new BigDecimal("99.99"));

        // Check that the right response is sent
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("Account bestaat niet.", result.getBody());

        // Check that there are no objects saved
        verify(transactionRepository, never()).save(any());
        verify(accountRepository, never()).save(any());
    }

    @Test
    public void testTransferMoneySameAccountIds() {
        ResponseEntity<String> result = controller.transferMoney(1, 1, new BigDecimal("100.01"));

        // Check that the right response is sent
        assertEquals(HttpStatus.BAD_REQUEST, result.getStatusCode());
        assertEquals("Je mag geen geld naar jezelf overmaken.", result.getBody());

        // Check that there are no objects saved
        verify(transactionRepository, never()).save(any());
        verify(accountRepository, never()).save(any());
    }
}
