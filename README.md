# Rabobank banking account assignment
Deze repository bevat een SpringBoot project waarmee een simpele bank applicatie wordt gesimuleerd.

## Requirement / Validations:
De applicatie voldoet aan de volgende requirements:

- A negative balance is not possible
- Account should contain at least some user details, card details and current balance
- One rest endpoint to see current available balance in all accounts
- One rest endpoint to withdraw money
- One rest endpoint to transfer money
- One credit card or debit card is linked with one account
- It should be able to audit transfers or withdrawals
- Front end part is not required
- Feel free to make some assumptions if needed & mention them in the code assignment

## Applicatie opstarten en gebruiken
- De repository kan gecloned worden en zoals elk ander SpringBoot project gerund worden vanuit een IDE.
- Er wordt gebruik gemaakt van een H2 in-memory database, dus er hoeft geen database worden aangemaakt.
- Zodra de applicatie gestart is kunnen de API's worden aangeroepen middels Swagger via de URL: http://localhost:8080/swagger-ui/ hier kunnen alle verschillende endpoints worden getest.
- Om wat testdata toe te voegen aan de database van de applicatie is er een endpoint gecreëerd in de test-controller. Met de addTestData methode zullen er 2 accountholders/accounts/cards/transactions worden aangemaakt.
- De accountIds die aangemaakt worden (zie response van addTestData request) kunnen vervolgens gebruikt worden in de verschillende requests van de transaction-controller, zoals transferMoney of withdrawMoney. 

## Opmerkingen
- Vanwege de reeds geinvesteerde tijd is er voor gekozen om alleen endpoints met business logica te testen. Zo zijn enpoints zoals getAllAccounts() of getAllTransactions() niet getest, aangezien die op dit moment enkel een doorgeefluik zijn voor de JPArepositories.
- Normaal zou ik de businesslogica niet verwerken in de controller, maar hier een servicelaag voor aanmaken. Vanwege het feit dat dit een assesment is en het onwenselijk is om een groot project op te leveren, heb ik er voor gekozen om de applicatie simpel te houden en deze extra laag niet toe te voegen.


